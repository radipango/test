FROM java:8
EXPOSE 8082
RUN mkdir -p /app/
ADD build/libs/test.jar /app/test.jar
ENTRYPOINT ["java", "-jar", "/app/test.jar"]
