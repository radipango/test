package com.pango.test.modal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
/*
 */
public class Email {
  private String toAddress;
  private String subject;
  private String body;

  public String getToAddress() {
    return toAddress;
  }

  public void setToAddress(final String toAddress) {
    this.toAddress = toAddress;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(final String subject) {
    this.subject = subject;
  }

  public String getBody() {
    return body;
  }

  public void setBody(final String body) {
    this.body = body;
  }
}
