package com.pango.test.Controller;

import com.pango.test.modal.BatchEmail;
import com.pango.test.modal.Email;
import com.pango.test.service.EmailService;
import java.net.URI;
import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@Getter
public class EmailController {

  @Autowired private EmailService emailService;

  @Autowired private Environment environment;

  @GetMapping("/emails/list")
  public List<Email> getAllEmails() {
    System.out.println(
        "Get Keys From db Secrets : " + environment.getProperty("spring.datasource.password"));
    System.out.println(
        "Get Keys From api Secrets : " + environment.getProperty("twilio.account.id"));
    System.out.println("Get Keys From aws Secrets : " + environment.getProperty("aws.access.key"));
    return emailService.getEmails();
  }

  @PostMapping("/batch/{batchId}/email")
  public ResponseEntity<Void> addEmailToBatch(
      @PathVariable long batchId, @RequestBody Email email) {

    BatchEmail batchEmail = emailService.addEmailToBatch(batchId, email);

    if (batchEmail == null) return ResponseEntity.noContent().build();

    final URI location =
        ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{batchId}")
            .buildAndExpand(batchEmail.getBatchId())
            .toUri();

    return ResponseEntity.created(location).build();
  }
}
