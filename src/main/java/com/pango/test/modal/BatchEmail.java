package com.pango.test.modal;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BatchEmail {

  private List<Email> emails;
  private long batchId;

  public List<Email> getEmails() {
    return emails;
  }

  public void setEmails(final List<Email> emails) {
    this.emails = emails;
  }

  public long getBatchId() {
    return batchId;
  }

  public void setBatchId(final long batchId) {
    this.batchId = batchId;
  }
}
