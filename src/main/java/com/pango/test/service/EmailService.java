package com.pango.test.service;

import com.pango.test.modal.BatchEmail;
import com.pango.test.modal.Email;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class EmailService {

  private static List<Email> emails = new ArrayList<>();

  private static List<BatchEmail> batchEmails = new ArrayList<>();

  static {

    // create user emails

    final Email userEmail1 = new Email();
    userEmail1.setToAddress("adi.rupesh@gmail.com");
    userEmail1.setSubject("Welcome Pango");
    userEmail1.setBody("Welcome Pango Body");

    final Email userEmail2 = new Email();
    userEmail2.setToAddress("adi.rupesh123@gmail.com");
    userEmail2.setSubject("Welcome Pango");
    userEmail2.setBody("Welcome Pango Body");
    emails.add(userEmail1);
    emails.add(userEmail2);

    // create a batch
    final BatchEmail batchEmail1 = new BatchEmail();
    batchEmail1.setBatchId(1l);
    batchEmail1.setEmails(emails);

    final BatchEmail batchEmail2 = new BatchEmail();
    batchEmail2.setBatchId(2l);
    batchEmail2.setEmails(emails);

    batchEmails.add(batchEmail1);
    batchEmails.add(batchEmail2);
  }

  public List<Email> getEmails() {
    return emails;
  }

  public List<BatchEmail> getBatchEmails() {
    return batchEmails;
  }

  public BatchEmail getBatchEmailByBatchId(final long batchId) {

    for (BatchEmail batchEmail : batchEmails) {
      if (batchEmail.getBatchId() == batchId) {
        return batchEmail;
      }
    }
    return null;
  }

  public BatchEmail addEmailToBatch(final long batchId, final Email email) {

    final BatchEmail batchEmail = getBatchEmailByBatchId(batchId);

    if (batchEmail == null) {
      final List<Email> tempList = new ArrayList<>();
      final BatchEmail batchEmail1 = new BatchEmail();
      batchEmail1.setBatchId(batchId);
      tempList.add(email);
      batchEmail1.setEmails(tempList);
      return batchEmail1;
    }
    batchEmail.getEmails().add(email);
    return batchEmail;
  }
}
